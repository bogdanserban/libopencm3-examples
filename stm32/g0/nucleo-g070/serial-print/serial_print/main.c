#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <string.h>
#include <stdio.h>

#define LED_PORT	GPIOA
#define LED_PIN		GPIO5

static void clock_setup(void){
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_USART2);
}

static void usart_setup(void) {
	
	usart_set_baudrate(USART2, 115200);
	usart_set_databits(USART2, 8);
	usart_set_stopbits(USART2, USART_STOPBITS_1);
	usart_set_mode(USART2, USART_MODE_TX);
	usart_set_parity(USART2, USART_PARITY_NONE);
	usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);
	usart_enable(USART2);
}

static void gpio_setup(void) {
	gpio_mode_setup(LED_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, LED_PIN);
	gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO2);
	gpio_set_af(GPIOA, GPIO_AF1, GPIO2);
}

static void serial_print(unsigned int USART, char str[]) {
	for(unsigned int i=0; i<strlen(str);i++){
		usart_send_blocking(USART, str[i]);
	}
}


int main(void){
	
	clock_setup();
	gpio_setup();
	usart_setup();
	
	char buf[100];
	uint32_t num1 = 13;

	while(1) {
		gpio_toggle(LED_PORT, LED_PIN);
		
		snprintf(buf, sizeof(buf), "Hello, the value is: %d\n", num1);
		serial_print(USART2, buf);


		for(int i=0; i<2000000; i++) {
		__asm__("nop");
		}
	}	
}

